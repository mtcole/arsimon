## AR Simon

This project was built using Unity version 2017.3.1p4 and Unity's ARKit plugin. There is a built XCode Project in the Projects/ folder. The game is designed to work just like the old Simon electronic game from Milton Bradley. The game only supports playing on vertical surfaces. 

If needed, you can enable the GeneratePlanes and PointCloudParticle game objects in the Main scene for better debugging.

## Crystal Clear

In addition, I also placed an APK of the latest version of the 2D action puzzle I am currently building titled Crystal Clear. There's no tutorial system yet, but the game is simple to learn. You just swipe a direction to change colors and then tap to shoot. Match the right color to the falling crystals.

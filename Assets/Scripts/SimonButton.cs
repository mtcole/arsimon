﻿using UnityEngine;

/// <summary>
/// Contains all logic regarding the actual buttons on the Simon board.
/// </summary>
[RequireComponent(typeof(MeshRenderer))]
public class SimonButton : MonoBehaviour
{
    [SerializeField] private Color _defaultColor;
    [SerializeField] private Color _activeColor;
    [SerializeField] private AudioSource _sound;
    private Material _material;
    
    public delegate void OnButtonPressEventHandler(SimonButton button);
    public event OnButtonPressEventHandler OnButtonPressed = delegate { };
    
    /// <summary>
    /// Gets or sets whether this button is currently accepting input.
    /// </summary>
    public bool IsLocked { get; set; }

    private void Awake()
    {
        _material = GetComponent<MeshRenderer>().material;
        Unpress();
    }

    /// <summary>
    /// Handles on mouse down events.
    /// </summary>
    private void OnMouseDown()
    {
        if (IsLocked)
        {
            return;
        }
        
        Press();
        OnButtonPressed(this);
    }

    /// <summary>
    /// Handles on mouse up events.
    /// </summary>
    private void OnMouseUp()
    {
        if (IsLocked)
        {
            return;
        }
        
        Unpress();
    }

    /// <summary>
    /// Presses the button down.
    /// </summary>
    public void Press()
    {
        _sound.Play();
        _material.color = _activeColor;
    }

    /// <summary>
    /// Unpresses the button.
    /// </summary>
    public void Unpress()
    {
        _material.color = _defaultColor;
    }
}
